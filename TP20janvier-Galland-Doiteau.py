import numpy as np
import matplotlib.pyplot as plt

def norme(vecteur):
    return np.sqrt(np.sum(np.square(vecteur)))

def scalaire(vecteur1, vecteur2):
    return np.sum(vecteur1 * vecteur2)

def cos(vecteur1, vecteur2):
    return scalaire(vecteur1, vecteur2) / (norme(vecteur1) * norme(vecteur2))

def sin(vecteur1, vecteur2):
    return (vecteur1[0] * vecteur2[1] - vecteur2[0] * vecteur1[1]) / (norme(vecteur1) * norme(vecteur2))

# Retourne un vecteur à partir de des coordonnées de 2 points
def vecteur(xa, ya, xb, yb):
    return np.array([xb - xa, yb - ya])


# Returne true si l'angle entre les deux vecteurs est positif
def signe_sin(vecteur1, vecteur2):
    return sin(vecteur1, vecteur2) >= 0

# Fonction utilitaire permettant de transformer le tracé d'une courbe en polygone
def to_polygon(tabX, tabY):
    tabX = np.append(tabX, tabX[0])
    tabY = np.append(tabY, tabY[0])
    return np.array([tabX, tabY])

# Retourne true si le polygone défini par les points en paramètre est convexe
def est_convexe(tabX, tabY):
    tailleTab = len(tabX)
    vecteur1 = vecteur(tabX[0], tabY[0], tabX[1], tabY[1])
    vecteur2 = vecteur(tabX[1], tabY[1], tabX[2], tabY[2])
    signe = signe_sin(vecteur1, vecteur2)
    for i in range(1, tailleTab):
        vecteur1 = vecteur(tabX[i], tabY[i], tabX[(i + 1) % tailleTab], tabY[(i + 1) % tailleTab])
        vecteur2 = vecteur(tabX[(i + 1) % tailleTab], tabY[(i + 1) % tailleTab], tabX[(i + 2) % tailleTab],
                           tabY[(i + 2) % tailleTab])
        if signe_sin(vecteur1, vecteur2) != signe:
            return False
    return True

# Retourne true si les deux droites (A,B) et (C,D) se croisent
def se_croisent(A, B, C, D):
    #
    vecteurAC = vecteur(A[0], A[1], C[0], C[1])
    vecteurAD = vecteur(A[0], A[1], D[0], D[1])
    vecteurBC = vecteur(B[0], B[1], C[0], C[1])
    vecteurBD = vecteur(B[0], B[1], D[0], D[1])
    #
    vecteurCA = vecteur(C[0], C[1], A[0], A[1])
    vecteurCB = vecteur(C[0], C[1], B[0], B[1])
    vecteurDA = vecteur(D[0], D[1], A[0], A[1])
    vecteurDB = vecteur(D[0], D[1], B[0], B[1])
    return signe_sin(vecteurAC, vecteurAD) != signe_sin(vecteurBC, vecteurBD) & \
           signe_sin(vecteurCA, vecteurCB) != signe_sin(vecteurDA, vecteurDB)

# Retourne le déterminant d'une matrice 2x2
def det2x2(matrice):
    return matrice[0][0] * matrice[1][1] - matrice[0][1] * matrice[1][0]

# Retourne le déterminant d'une matrice 3x3
def det3x3(matrice):
    return matrice[0][0] * det2x2(np.array([[matrice[1][1], matrice[1][2]], [matrice[2][1], matrice[2][2]]])) -\
    matrice[1][0] * det2x2(np.array([[matrice[0][1], matrice[0][2]], [matrice[2][1], matrice[2][2]]])) +\
    matrice[2][0] * det2x2(np.array([[matrice[0][1], matrice[0][2]], [matrice[1][1], matrice[1][2]]]))

# Retourne la matrice inverse d'une matrice 2x2
def inverse_matrice2x2(matrice2x2):
    detM = det2x2(matrice2x2)
    if (detM == 0):
        print("Déterminant nul")
        return
    matriceInverse = np.zeros(matrice2x2.shape)
    matriceInverse[0][0] = matrice2x2[1][1] / detM
    matriceInverse[1][0] = - matrice2x2[1][0] / detM
    matriceInverse[0][1] = -  matrice2x2[0][1] / detM
    matriceInverse[1][1] = matrice2x2[0][0] / detM
    return matriceInverse

X = np.array([2, 3])
Y = np.array([4, 1])
Z = np.array([4, 6])

print("Test fonction cosinus et sinus")
print(cos(X, Y))
print(sin(X, Y))
print(cos(X, Z))
print(sin(X, Z))

print("Test de la propriété cos² + sin² = 1")
print(pow(cos(X, Y), 2) + pow(sin(X, Y), 2))
print(pow(cos(X, Z), 2) + pow(sin(X, Z), 2))

tabX = np.array([1, 2, 4, 5])
tabY = np.array([1, 3, 3, 1])
tabX2 = np.array([1, 2, 3, 5])
tabY2 = np.array([1, 3, 1.5, 1])

print("Test des polygones convexes")

print("Polygone 1 : ", est_convexe(tabX, tabY))
poly = to_polygon(tabX, tabY)
plt.plot(poly[0], poly[1])
plt.ylabel('Polygone 1')
plt.show()

print("Polygone 2 : ", est_convexe(tabX2, tabY2))
poly = to_polygon(tabX2, tabY2)
plt.plot(poly[0], poly[1])
plt.ylabel('Polygone 2')
plt.show()

A = np.array([2, 4])
B = np.array([4, 1])
C = np.array([1, 1])
D = np.array([5, 2])

print("Test croisement des droites")
print(se_croisent(A, B, C, D))
plt.plot([A[0], B[0]], [A[1], B[1]], 'r')
plt.plot([C[0], D[0]], [C[1], D[1]], 'b')
plt.show()

D = np.array([3, 2])
print(se_croisent(A, B, C, D))
plt.plot([A[0], B[0]], [A[1], B[1]], 'r')
plt.plot([C[0], D[0]], [C[1], D[1]], 'b')
plt.show()

matrice2x2 = np.array([[5, 7], [2, 3]])
matrice3x3 = np.array([[2, 3, 0], [1, -2, 4], [5, 0, 1]])

print("Test matrice inverse 2x2")
print("matrice initiale : ")
print(matrice2x2)
print("matrice inverse : ")
print(inverse_matrice2x2(matrice2x2))
print("matrice inverse de la matrice inverse : ")
print(inverse_matrice2x2(inverse_matrice2x2(matrice2x2)))