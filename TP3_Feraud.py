import numpy as np
import matplotlib.pyplot as plt

# Génère 30 points aléatoires
n = 30
xs = np.random.rand(n)
ys = np.random.rand(n)


def barycentre(xs, ys):
    mx = np.mean(xs)
    my = np.mean(ys)
    return [mx, my]


def changer_repere(xs, ys, B):
    Xs = xs - B[0]
    Ys = ys - B[0]
    return (Xs, Ys)


def matrice_correlation(xs, ys):
    B = barycentre(xs, ys)
    Xs, Ys = changer_repere(xs, ys, B)
    mat = [[sum(Xs * Xs), sum(Xs * Ys)], [sum(Xs * Ys), sum(Ys * Ys)]]
    return mat


def resoudre_equation(a, b, c):
    delta = b ** 2 - (4 * a * c)
    if delta > 0:
        s1 = (-b - np.sqrt(delta)) / (2 * a)
        s2 = (-b + np.sqrt(delta)) / (2 * a)
        return [s1, s2]
    if delta == 0:
        return [-b / (2 * a)]
    else:
        return []


def diagonaliser(xs, ys):
    A = matrice_correlation(xs, ys)
    [[a, b], [_, c]] = A
    [lambda1, lambda2] = resoudre_equation(1, -(a + c), a * c - b ** 2)

    def calculer_vecteur_propre(la):
        if a - la == 0 and b == 0:
            u = [1, 0]
        else:
            u = [b, -(a - la)]
        return u

    if lambda2 > lambda1:
        lambda1, lambda2 = lambda2, lambda1
    assert lambda1 > 0 and lambda2 >= 0

    u1 = calculer_vecteur_propre(lambda1)
    u2 = calculer_vecteur_propre(lambda2)

    return lambda1, lambda2, u1, u2


'''
Visualise les points et les vecteurs propres.
'''


def visualiser(xs, ys, c):
    (l1, l2, u1, u2) = diagonaliser(xs, ys)

    B = np.array(barycentre(xs, ys))

    t1 = [B, u1 + B]
    t2 = [B, u2 + B]

    plt.axis('equal')

    print("λ1 = ", l1)
    print("λ2 = ", l2)

    plt.plot([t1[0][0], t1[1][0]], [t1[0][1], t1[1][1]], color='black')
    plt.plot([t2[0][0], t2[1][0]], [t2[0][1], t2[1][1]], color='black')

    plt.scatter(xs, ys, color=c)
    plt.show()


'''
Retourne une coordonnée décalée d'un nombre aléatoire
'''
def decaler(x):
    nx = x + np.random.uniform(-0.1, 0.1)
    return nx


colors = ['red', 'blue', 'green', 'orange', 'purple']

for i in range(0, 10):
    visualiser(xs, ys, colors[i % 5])
    xs = [decaler(x) for x in xs]
    ys = [decaler(y) for y in ys]
