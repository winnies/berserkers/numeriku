# Loïc Escales & Félix Feraud

import numpy as np
import math
import matplotlib.pyplot as plt
import numpy.polynomial as nppol

from scipy import misc

# 1

A = np.array([[90, 1], [1, 38.5]])


def calcul_vecteur_propre(A, mu, epsilon):
    v = np.random.rand(1, 2)[0]
    n = np.linalg.norm(v)
    v = v / n

    I = np.identity(A.shape[0])

    while True:
        g = np.linalg.inv(A - np.dot(mu, I)) @ v
        r = g / np.linalg.norm(g)

        if np.linalg.norm(r - v) < epsilon:
            return r

        v = r


v = calcul_vecteur_propre(A, 0, 0.00001)
print(v)
print(np.linalg.norm(np.dot(A, v)))


def calcul_vecteur_propre_1(A, epsilon):
    v = np.random.rand(1, 2)[0]
    n = np.linalg.norm(v)
    v = v / n

    lm = np.transpose(v) @ A @ v

    while True:
        petit_v = (lm * A) @ v
        v = petit_v / np.linalg.norm(petit_v)
        n_lm = np.transpose(v) @ A @ v / (np.transpose(v) @ v)

        if abs(n_lm - lm) / lm < epsilon:
            return v, n_lm

        lm = n_lm


v, lm = calcul_vecteur_propre_1(A, 0.000001)
print(v)
print(lm)

# 2

def newton(f, nbI):
    x = 0
    for _ in range(nbI):
        x = x - (f(x) / misc.derivative(f, x))
    return x


def racines(f, degree, nbI):
    if degree > 0:
      r = newton(f, nbI)
      return [r] + racines(lambda x: f(x) / (x - r), degree - 1, nbI)
    else:
      return []


f = lambda x: pow(x, 3) + 33 * pow(x, 2) + 327 * x + 935
for r in racines(f, 3, 50):
  print(f(r))
