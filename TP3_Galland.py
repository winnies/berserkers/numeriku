import numpy as np
import matplotlib.pyplot as plt
import math
import time

# Borne supérieure pour la valeur des coordonnées des points
rang = 10

def barycentre(tabX, tabY):
    xMean = np.mean(tabX)
    yMean = np.mean(tabY)
    return (xMean, yMean)

def matrice_correlation(tabX, tabY):
    G = barycentre(tabX, tabY)
    tabXCentre = tabX - G[0]
    tabYCentre = tabY - G[1]
    coef1 = np.sum(np.power(tabXCentre, 2))
    coef2 = np.sum(np.multiply(tabXCentre, tabYCentre))
    coef3 = np.copy(coef2)
    coef4 = np.sum(np.power(tabYCentre, 2))
    return np.array([[coef1, coef2], [coef3, coef4]])

def calcul_delta(a, b, c):
    return b*b-4*a*c

def solution_second_degre(a, b, c):
    delta = calcul_delta(a, b, c)
    if delta > 0:
        racineDelta = math.sqrt(delta)
        resultat = [(-b-racineDelta)/(2*a), (-b+racineDelta)/(2*a)]
    elif delta == 0:
        resultat = [-b/(2*a)]
    else:
        resultat = []
    return resultat

def valeurs_propres(matrice):
    a = 1
    b = -(matrice[0][0] + matrice[1][1])
    c = (matrice[0][0] * matrice[1][1]) - math.pow(matrice[0, 1], 2)
    return solution_second_degre(a, b, c)

# Retourne les vecteurs propres associés aux valeurs propres en paramètre
# (leur norme dépendant de la valeur propre associée et de la variable 'rang')
def vecteurs_propres(matrice, valeur_propre1, valeur_propre2):
    matrice_id = np.array([[1, 0], [0, 1]])
    valeur_propre_princ = max(valeur_propre1, valeur_propre2)
    valeur_propre_sec = min(valeur_propre1, valeur_propre2)
    matrice_intermediaire = matrice - matrice_id * valeur_propre_princ
    a = matrice_intermediaire[0][0]
    b = matrice_intermediaire[0][1]
    norme_pr = rang * valeur_propre_princ / (valeur_propre1 + valeur_propre2)
    norme_sec = rang * valeur_propre_sec / (valeur_propre1 + valeur_propre2)
    if a == 0 and b == 0:
        return np.array([[1, 0], [0, 1]])
    else:
        if -b < 0:
            return np.array([np.array([b, -a]) * norme_pr, np.array([a, b]) * norme_sec])
        else:
            return np.array([np.array([-b, a]) * norme_pr, np.array([-a, -b]) * norme_sec])

for i in range(10):
    tabX = np.random.randint(rang, size=10)
    tabY = np.random.randint(rang, size=10)

    bary = barycentre(tabX, tabY)
    print("Barycentre : ", bary)
    matrice_corr = matrice_correlation(tabX, tabY)
    print("Matrice de corrélation : ", matrice_corr)
    valeurs_pr = valeurs_propres(matrice_corr)
    print("Valeurs propres : ", valeurs_propres(matrice_corr))
    vecteur_pr = vecteurs_propres(matrice_corr, valeurs_pr[0], valeurs_pr[1])
    print("Vecteurs propres : ", vecteur_pr)
    print("-----------------------------------------------------------------")

    origin = bary[0], bary[1]
    plt.scatter(tabX, tabY)
    plt.scatter(bary[0], bary[1])
    plt.quiver(*origin, vecteur_pr[:, 0], vecteur_pr[:, 1], color=['r', 'b'], angles='xy', scale_units='xy', scale=100)
    plt.axis('equal')
    plt.show(block=False)
    time.sleep(1)
    plt.close()