#!/usr/bin/env python
# coding: utf-8

# Par : Loïc Escales & Félix Féraud

import numpy as np
import matplotlib.pyplot as plt

#NORME, PRODUIT SCALAIRE, PRODUIT VECTORIEL

def norme(v):
    return np.sqrt(pow(v[0], 2) + pow(v[1], 2))

def produit_scalaire(a, b):
    return a[0] * b[0] + a[1] * b[1]

def produit_vectoriel(a, b):
    return a[0] * b[1] - a[1] * b[0]


# SINUS ET COSINUS

# u et v vecteurs
def cosinus(u, v):
    return produit_scalaire(u, v) / (norme(u) * norme(v))

def sinus(u, v):
    return produit_vectoriel(u, v) / (norme(u) * norme(v))

def angle(u, v):
    return np.arccos(cosinus(u, v))

u = [3, 7]
v = [5, 8]

c = cosinus(u, v)
s = sinus(u, v)

print('cos(u,v) = ', c)
print('sin(u,v) = ', s)
print('-------\ncos(u,v)² + sin(u,v)² = ' ,c**2 + s**2)
print('angle(u,v) = ', angle(u,v))


# POLYGONE CONVEXE

x = [1, 1, 1.2, 2]
y = [1, 2, 1.2, 1]

# x et y coordonnées des points du polygone
def polygone_convexe(x, y):
    n = len(x)
    v = []
    for i in range(n):
        v.append([x[(i+1)%n] - x[i], y[(i+1)%n] - y[i]])
    
    s = sinus(v[0], v[1])
    sign = np.sign(s)
    convexe = True
    for i in range(n):
        s = sinus(v[i], v[(i+1)%n])
        newsign = np.sign(s)
        if(newsign != sign):
            convexe = False
    return convexe

c = polygone_convexe(x, y)

if(c):
    print('Polygone Convexe')
else:
    print('Polygone non-convexe')


plt.plot(x, y, color = 'green')
plt.show()


# INTERSECTION DE SEGMENTS

# Crée un vecteur à partir d'un segment AB
def vecteur_segment(A, B):
    return [B[0] - A[0], B[1] - A[1]]

plt.axis('equal')

# Segments AB et CD 
A = [2, 2]
B = [1, 1]

C = [1.5, 0]
D = [2, 1.3]

# Segments EF et GH

E = [3, 3]
F = [4, 4]

G = [3.5, 2]
H = [3, 4.1]


plt.plot([A[0], B[0]], [A[1], B[1]], color = 'red')
plt.plot([C[0], D[0]], [C[1], D[1]], color = 'blue')

plt.plot([E[0], F[0]], [E[1], F[1]], color = 'green')
plt.plot([G[0], H[0]], [G[1], H[1]], color = 'orange')

# Renvoie vrai si AB et CD se coupent, faux sinon.
def intersection_segments(A, B, C, D):
    AB = vecteur_segment(A, B)
    AC = vecteur_segment(A, C)
    AD = vecteur_segment(A, D)

    CD = vecteur_segment(C, D)
    CA = vecteur_segment(C, A)
    CB = vecteur_segment(C, B)

    pABAC = produit_vectoriel(AB, AC)
    pABAD = produit_vectoriel(AB, AD)
    pCDCA = produit_vectoriel(CD, CA)
    pCDCB = produit_vectoriel(CD, CB)
    
    return (np.sign(pABAC) != np.sign(pABAD)) and (np.sign(pCDCA) != np.sign(pCDCB))
    
if(intersection_segments(A, B, C, D)):
    print('Les segments Rouge et Bleu se croisent')
else:
    print('Les segments Rouge et Bleu ne se croisent pas')
    
if(intersection_segments(E, F, G, H)):
    print('Les segments Vert et Orange se croisent')
else:
    print('Les segments Vert et Orange ne se croisent pas')

# INVERSION DE MATRICE


# 2x2

M = [[2, 3], [4, 5]]

def determinant2x2(M):
    return M[0][0] * M[1][1] - M[1][0] * M[0][1]


def mat_inverse2x2(M):
    d = determinant2x2(M)
    M2 = np.array([[M[1][1], -M[0][1]], [-M[1][0], M[0][0]]])
    return (1/d) * M2

print('\n[2x2]\n')
print('Matrice Originale : \n', M)
inv = mat_inverse2x2(M)
print('\nMatrice Inverse : \n', inv)
inv_inv = mat_inverse2x2(inv)
print('\nInverse de l\'Inverse : \n', inv_inv)

# 3x3

M = np.array([[-2, 2, -3], [-1, 1, 3], [2, 0, -1]])

def determinant3x3(M):
    [[a, b, c], [d, e, f], [g, h, i]] = M
    M1 = [[e, f], [h, i]]
    M2 = [[b, c], [h, i]]
    M3 = [[b, c], [e, f]]
    det = a * determinant2x2(M1) - d * determinant2x2(M2) + g * determinant2x2(M3)
    return det

def mat_inverse3x3(M):
    det = determinant3x3(M)
    assert det != 0
    
    mat_inv = np.zeros(M.shape)
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            sm = sous_matrice(M, i, j)
            det_sm = determinant2x2(sm)
            mat_inv[i][j] = det_sm
            
    mat_inv = mat_inv * matrice_signes(3,3)
    mat_inv = (1 / det) * np.transpose(mat_inv)
    
    return mat_inv


def sous_matrice(M, i, j):
    new = []
    for l in range(M.shape[0]):
        if l == i:
            continue
            
        t = []
        for c in range(M.shape[1]):
            if c != j:
                t.append(M[l][c])
                
        new.append(t)
        
    return np.array(new)

def matrice_signes(i, j):
    m = np.zeros((i, j))
    signe = 1
    for l in range(i):
        for c in range(j):
            m[l][c] = signe
            signe = -signe
    return m

print('\n[3x3]\n')
print('Matrice Originale : \n', M)
inv = mat_inverse3x3(M)
print('\nMatrice Inverse : \n', inv)
inv_inv = mat_inverse3x3(inv)
print('\nInverse de l\'Inverse : \n', inv_inv)





