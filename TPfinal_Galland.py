# Thomas Galland & Loïc Escales

import sklearn.datasets
import numpy.random
import numpy as np
import math

dim = 10

# Retourne une matrice définie positive de dimention dim
A = sklearn.datasets.make_spd_matrix(dim)
b = numpy.random.rand(1, dim)[0]

#A = np.array([[20,1], [1,30]])
#b = np.array([10, 55])

x = np.zeros(dim)
p = np.copy(b)
r = np.copy(b)

print(" A : ", A)
print(" b : ", b)
print(" x : ", x)
print(" p : ", p)
print(" r : ", r)
print("_______________")

nb_iterations = 0

while True:

	denom = (p @ A @ p)
	if denom == 0:
		break
	alpha = (r @ r) / denom

	x_n = x + (alpha * p)
	r_n = r - alpha * A @ p
	beta = (r_n @ r_n) / (r @ r)
	p_n = r_n + beta * p

	if np.allclose(A @ x, b):
		break

	x = x_n
	p = p_n
	r = r_n

	nb_iterations += 1

	print(" x : ", x)

print("Valeur de x finale : ", x)
print("Nombre d'itérations : ", nb_iterations)
print("A * x = ", A @ x)
